package com.example.springboot001.Repository;

import com.example.springboot001.Entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long> {
}
